/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package player;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author ff
 */
public class Player {
    private String fecha;
    private String nombre;
    private int puntaje;
    private int tiempo;
    private int vidas;
    private Auto auto;

    public Player(String nombre, int puntaje, int vidas) {
        this.nombre = nombre;
        this.puntaje = puntaje;
        this.vidas = vidas;
        this.tiempo = 0;
        this.auto = null;
        this.fecha = new Date().toString();
    }

    public Player(String nombre) {
        this.nombre = nombre;
        this.puntaje = 0;
        this.tiempo = 0;
        this.vidas = 3;
        this.auto = null;
        this.fecha = new Date().toString();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public Auto getAuto() {
        return auto;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    public int getVidas() {
        return vidas;
    }

    public void setVidas(int vidas) {
        this.vidas = vidas;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
       
    
    

    @Override
    public String toString() {
        return nombre + "," + puntaje + "," + tiempo+ ","+ fecha;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.nombre);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
