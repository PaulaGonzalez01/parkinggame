/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package player;

import java.util.Objects;

/**
 *
 * @author pcgfd
 */
public class Auto extends Objeto {
    private boolean parqueado;
    private String color;
    
    public Auto(String rutaImagen, Coordenada posicion) {
        super(rutaImagen, posicion);
        parqueado = false;
    }

    public boolean isParqueado() {
        return parqueado;
    }

    public void setParqueado(boolean parqueado) {
        this.parqueado = parqueado;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.color);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Auto other = (Auto) obj;
        if (!Objects.equals(this.color, other.color)) {
            return false;
        }
        return true;
    }
    
    
    
    
}
