/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package player;

/**
 *
 * @author pcgfd
 */
public class ZonaParqueo extends Objeto{
    private boolean lleno;

    public ZonaParqueo(String rutaImagen, Coordenada posicion) {
        super(rutaImagen, posicion);
    }

    public boolean isLleno() {
        return lleno;
    }

    public void setLleno(boolean lleno) {
        this.lleno = lleno;
    }
    
    
}
