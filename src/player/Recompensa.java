/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package player;

/**
 *
 * @author pcgfd
 */
public class Recompensa extends Objeto {
    private int tiempo;
    private int puntos;

    public Recompensa(String rutaImagen, Coordenada posicion) {
        super(rutaImagen, posicion);
        tiempo = 15;
        puntos = 100;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
    
    
}
