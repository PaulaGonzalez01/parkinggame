/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package player;

/**
 *
 * @author pcgfd
 */
public abstract class PremioEspecial extends Objeto{
    protected int tiempo;
    protected boolean activo;
    

    public PremioEspecial(String rutaImagen, Coordenada posicion) {
        super(rutaImagen, posicion);
        tiempo = 15;
        activo = true;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    
    
    
    
}
