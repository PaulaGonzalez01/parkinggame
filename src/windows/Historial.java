/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import player.Player;

/**
 *
 * @author ff
 */
public class Historial {

    private Label title;
    private TableView listView;
    private VBox root;
    private ObservableList<Player> ob = FXCollections.observableArrayList();

    public Historial() {
        inicializar();
        setTableView();
    }

    private void inicializar() {
        title = new Label("Jugadores");
        listView = new TableView();
        listView.setId("table");
        root = new VBox(title, listView);
    }

    public VBox getRoot() {
        return root;
    }

    public ArrayList<Player> getPlayers() {
        ArrayList<Player> players = new ArrayList<>();
        try (BufferedReader bf = new BufferedReader(new FileReader("jugadores.txt"))) {
            while (bf.ready()) {
                String[] list = bf.readLine().split(",");
                Player p = new Player(list[0]);
                p.setPuntaje(Integer.parseInt(list[1]));
                p.setTiempo(Integer.parseInt(list[2]));
                p.setFecha(list[3]);
                players.add(p);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Historial.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Historial.class.getName()).log(Level.SEVERE, null, ex);
        }
        return players;
    }

    private void setTableView() {
        TableColumn<Player, String> nameCol = new TableColumn<>("Nombre");
        nameCol.setMinWidth(200);
        nameCol.setCellValueFactory(new PropertyValueFactory("nombre"));

        TableColumn<Player, Integer> scoreCol = new TableColumn<>("Puntaje");
        scoreCol.setMinWidth(100);
        scoreCol.setCellValueFactory(new PropertyValueFactory("puntaje"));

        TableColumn<Player, Integer> timeCol = new TableColumn<>("Tiempo");
        timeCol.setMinWidth(100);
        timeCol.setCellValueFactory(new PropertyValueFactory("tiempo"));

        TableColumn<Player, String> dateCol = new TableColumn<>("Fecha");
        dateCol.setMinWidth(400);
        dateCol.setCellValueFactory(new PropertyValueFactory("fecha"));

        ob.addAll(getPlayers());
        listView.setItems(ob);
        listView.getColumns().addAll(nameCol, scoreCol, timeCol, dateCol);

    }

}
