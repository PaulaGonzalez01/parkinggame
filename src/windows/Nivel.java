/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import hilos.HiloPremio;
import hilos.HiloRecompensa;
import hilos.HiloTiempo;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import player.Auto;
import player.Coordenada;
import player.Freezer;
import player.Live;
import player.Obstaculo;
import player.Player;
import player.PremioEspecial;
import player.SuperPower;
import player.ZonaParqueo;
import sound.Sound;

/**
 *
 * @author ff
 */
public class Nivel {

    public static boolean interrupt = false;
    public static boolean superp = false;
    public static boolean quitar = false;
    public static boolean quitar2 = false;
    private HiloTiempo ht;
    private Thread h_tiempo;
    private Thread h_premio1;
    private Thread h_recom1;
    private Thread h_premio2;
    private Thread h_recom2;
    private Thread h_premio3;
    private Thread h_recom3;
    
    private Sound crash;
    private Sound recom;
    private Sound free;
    private Sound lifes;
    private Sound gano;
    private Sound perdio;
    private Sound power;
    private Sound nivel1;
    
    

    private Label mensaje;
    private GridPane context;
    private HBox upBox;
    private Label timeLabel;
    private Label lifesLabel;
    private Label PuntajeLabel;
    private Label avisosLabel;
    private Label timeLabel2;
    private Label lifesLabel2;
    private Label PuntajeLabel2;
    private Label title;
    private HBox timeBox;
    private HBox lifeBox;
    private HBox PuntajeBox;
    private VBox playerInfo;
    private VBox timeInfo;
    private VBox layoutRoot;
    private ZonaParqueo zona;
    private Rectangle r_zona;

    private int puntaje;
    private int vidas;
    private int tiempo;

    private ArrayList<Coordenada> posiciones;
    private ArrayList<Rectangle> fig_ob;
    private ArrayList<Obstaculo> obstaculos;
    private ArrayList<PremioEspecial> premiosEspeciales;

    //ELEMENTOS DEL JUGADOR. PUNTO DE PARTIDA: 0 x 50
    private Player jugador;
    private Auto autoJugador;
    private Rectangle r_jugador;
    private Coordenada posJugador;

    private final EventHandler<KeyEvent> handler = (KeyEvent event) -> {
        if (null != event.getCode()) {
            switch (event.getCode()) {
                case RIGHT:
                    r_jugador.setTranslateX(r_jugador.getTranslateX() + 10);
                    choqueObs();
                    break;
                case LEFT:
                    r_jugador.setTranslateX(r_jugador.getTranslateX() - 10);
                    choqueObs();
                    break;
                case UP:
                    r_jugador.setTranslateY(r_jugador.getTranslateY() - 10);
                    choqueObs();
                    break;
                case DOWN:
                    r_jugador.setTranslateY(r_jugador.getTranslateY() + 10);
                    choqueObs();
                    break;
                default:
                    break;
            }
        }
        event.consume();
    };

    public Nivel() {
        this.jugador = Seleccion.jugador;
        getSounds();
        encerar();
        crearZona();
        setCoorJugador();
        inicializar();
        asignarPosOb(10);
        crearHiloT();
        generarHilos();
    }
    
    private void getSounds(){
        crash = new Sound("crash.mp3");
        recom = new Sound("recom.mp3");
        free = new Sound("free.mp3");
        lifes = new Sound("heart.mp3");
        gano = new Sound("win.mp3");
        perdio = new Sound("lose.mp3");
        power = new Sound("power.mp3");
        nivel1 = new Sound("nivel1.mp3");
    }

    private final void inicializar() {
        
        
        nivel1.play();
        mensaje = new Label();
        mensaje.setId("title_main");
        context = new GridPane();
        title = new Label("Nivel 1");
        title.setId("title_main");
        timeLabel = new Label("Tiempo: ");
        timeLabel.setId("title_main");
        PuntajeLabel = new Label("Puntaje: ");
        PuntajeLabel.setId("title_main");
        lifesLabel = new Label("Vidas: ");
        lifesLabel.setId("title_main");

        timeLabel2 = new Label();
        timeLabel2.setId("title_main");
        PuntajeLabel2 = new Label();
        PuntajeLabel2.setId("title_main");
        PuntajeLabel2.setText(String.valueOf(puntaje));
        lifesLabel2 = new Label();
        lifesLabel2.setId("title_main");
        lifesLabel2.setText(String.valueOf(vidas));

        lifeBox = new HBox(lifesLabel, lifesLabel2);
        PuntajeBox = new HBox(PuntajeLabel, PuntajeLabel2);
        timeBox = new HBox(timeLabel, timeLabel2);
        playerInfo = new VBox(lifeBox, PuntajeBox, mensaje);
        timeInfo = new VBox(title, timeBox);

        upBox = new HBox(playerInfo, timeInfo);
        upBox.setId("upper_lay");

        context.setId("pane");

        context.add(r_zona, (int) zona.getPosicion().getX(), (int) zona.getPosicion().getY());

        r_jugador.setWidth(80);
        r_jugador.setHeight(70);
        r_jugador.setArcWidth(30);
        r_jugador.setArcHeight(30);
        r_jugador.setStroke(Color.BLACK);
        context.getChildren().add(r_jugador);

        layoutRoot = new VBox(upBox, context);
        layoutRoot.setStyle("-fx-background-image: url(/images/paneb.jpg)");

    }

    private Coordenada getRandomPos() {

        int x = randomNumberInRange(1, 8);
        int y = randomNumberInRange(0, 8);
        Coordenada c = new Coordenada(x, y);
        System.out.println(c);
        while (c.equals(posJugador) || posiciones.contains(c) || c.equals(zona.getPosicion())) {
            System.out.println("Dentro");
            System.out.println("Coord actual: " + c);
            x = randomNumberInRange(0, 8);
            y = randomNumberInRange(0, 8);
            c.setX(x);
            c.setY(y);
        }
        posiciones.add(c);

        return c;

    }

    protected VBox getRoot() {
        return layoutRoot;
    }

    private void asignarPosOb(int n) {
        System.out.println("Asignando posiciones .....");
        for (int i = 0; i < n; i++) {
            String ruta = randomRuteObs();
            Coordenada coor = getRandomPos();
            Rectangle r = new Rectangle(100, 100, new ImagePattern(new Image(ruta)));
            r.setArcWidth(20);
            r.setArcHeight(20);
            r.setStroke(Color.RED);
            r.setId("obstaculo");
            Obstaculo ob = new Obstaculo(ruta, coor);
            obstaculos.add(ob);

            fig_ob.add(r);

            context.add(r, (int) coor.getX(), (int) coor.getY());
        }

        Freezer f = new Freezer("/images/freezer.png", getRandomPos());
        Live l = new Live("/images/corazon.png", getRandomPos());
        SuperPower sp = new SuperPower("/images/superpower.png", getRandomPos());
        premiosEspeciales.add(f);
        premiosEspeciales.add(l);
        premiosEspeciales.add(sp);
    }

    private String randomRuteObs() {
        System.out.println("randomRoute......");
        String[] rutasO = {"/images/gato.png", "/images/cubo2.png", "/images/pelota.png", "/images/sem.png"};
        Random r = new Random();
        int al = r.nextInt(4);
        return rutasO[al];
    }

    public EventHandler<KeyEvent> getHandler() {
        return handler;
    }

    private void choqueObs() {
        System.out.println("choqueObs......");
        for (Rectangle r : fig_ob) {
            if (r_jugador.getBoundsInParent().intersects(r.getBoundsInParent())) {

                switch (r.getId()) {
                    case "obstaculo":

                        if (!superp) {
                            lifesLabel2.setText(String.valueOf(--vidas));
                            System.out.println("Choque");
                            crash.play();
                            r_jugador.setTranslateX(0);
                            r_jugador.setTranslateY(50);
                        }

                        if (vidas == 0) {
                            perdio.play();
                            guardarDatosJugador();
                            guardarArchivo();
                            lose();
                        }
                        break;
                    case "puntos":
                        recom.play();
                        puntaje += 100;
                        PuntajeLabel2.setText(String.valueOf(puntaje));
                        context.getChildren().remove(r);
                        quitar2 = true;
                        break;
                    case "vida":
                        lifes.play();
                        System.out.println("Vida");
                        lifesLabel2.setText(String.valueOf(++vidas));

                        mensaje.setText("¡Aumentan tus vidas!");
                        context.getChildren().remove(r);
                        quitar = true;
                        break;
                    case "freezer":
                        free.play();
                        interrupt = true;
                        mensaje.setText("¡Freezer activado!");
                        context.getChildren().remove(r);
                        quitar = true;
                        break;
                    case "sp":
                        power.play();
                        superp = true;
                        mensaje.setText("¡Super poder activado!");
                        context.getChildren().remove(r);
                        quitar = true;
                        break;
                    case "zona":
                        gano.play();
                        guardarDatosJugador();
                        win(jugador);
                        break;
                    default:
                        break;
                }

            }

        }
    }

    private PremioEspecial getRandomPre() {
        System.out.println("GetRandomPre.....");
        Random r = new Random();
        int x = r.nextInt(3);
        return premiosEspeciales.get(x);

    }

    private void generarHilos() {
        System.out.println("Generando hilos.... ");
        h_premio1 = new Thread(new HiloPremio(context, getRandomPre(), fig_ob));
        h_recom1 = new Thread(new HiloRecompensa(getRandomPos(), context, fig_ob));
        h_premio2 = new Thread(new HiloPremio(context, getRandomPre(), fig_ob));
        h_premio3 = new Thread(new HiloPremio(context, getRandomPre(), fig_ob));
        h_recom2 = new Thread(new HiloRecompensa(getRandomPos(), context, fig_ob));
        h_recom3 = new Thread(new HiloRecompensa(getRandomPos(), context, fig_ob));

        h_premio1.start();
        h_recom1.start();
        h_premio2.start();
        h_premio3.start();
        h_recom2.start();
        h_recom3.start();

    }

    public static int randomNumberInRange(int min, int max) {
        System.out.println("Randon range....");
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    private void win(Player jugador) {
        System.out.println(jugador);
        nivel1.stop();
        h_tiempo.stop();
        System.out.println("gano.....");
        Alert a = new Alert(Alert.AlertType.CONFIRMATION);
        a.setTitle("Ganador");
        a.setHeaderText("Has completado el nivel 1. ¿Desea seguir jugando?");
        a.setContentText("Jugador: " + jugador.getNombre() + "\nPuntaje: " + jugador.getPuntaje() + "\nTiempo: " + jugador.getTiempo());

        // option != null.
        Optional<ButtonType> option = a.showAndWait();

        if (option.get() == ButtonType.OK) {
            Nivel2 n2 = new Nivel2();
            Scene s = new Scene(n2.getRoot(), 900, 900);
            s.setOnKeyPressed(n2.getHandler());
            s.getStylesheets().add("/windows/stylesCSS.css");

            Main.stage2.setScene(s);
        } else if (option.get() == ButtonType.CANCEL) {
            Main.stage2.close();
        }

    }

    private void lose() {
        System.out.println(jugador);
        nivel1.stop();
        h_tiempo.stop();
        System.out.println("perdio....");
        Stage st = new Stage();
        GameOver go = new GameOver(st, Main.stage2);

        Scene s = new Scene(go.getRoot(), 500, 500);
        st.setScene(s);
        st.show();
    }

    private final void crearZona() {
        System.out.println("Creando zona....");
        zona = new ZonaParqueo("/images/zona.png", new Coordenada(8, 5));
        r_zona = new Rectangle(70, 70, new ImagePattern(new Image(zona.getRutaImagen())));
        r_zona.setId("zona");
        fig_ob.add(r_zona);
    }

    private void setCoorJugador() {
        System.out.println("Seteando jugador....");
        autoJugador.setPosicion(posJugador);
    }

    private void guardarDatosJugador() {
        jugador.setPuntaje(puntaje);
        jugador.setTiempo(ht.getTiempo()+ jugador.getTiempo());
        jugador.setAuto(autoJugador);
    }

    private void guardarArchivo() {
        try (PrintWriter p = new PrintWriter(new FileWriter("jugadores.txt", true), false)) {
            System.out.println("Guardando jugador " + jugador.getNombre());
            p.println(jugador.toString());

        } catch (IOException ex) {
            System.err.println("No se guardó el jugador");
        }
    }

    private void crearHiloT() {
        ht = new HiloTiempo(timeLabel2, puntaje);
        h_tiempo = new Thread(ht);
        h_tiempo.start();
    }

    public final void encerar() {
        vidas = 3;
        puntaje = 0;
        autoJugador = Seleccion.carPlayer;
        r_jugador = Seleccion.r_auto;
        posJugador = new Coordenada(0, 0);
        posiciones = new ArrayList<>();
        fig_ob = new ArrayList<>();
        obstaculos = new ArrayList<>();
        premiosEspeciales = new ArrayList<>();

        posiciones.add(posJugador);
    }

}
