/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import java.util.ArrayList;
import java.util.Random;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import player.Auto;
import player.Player;

/**
 *
 * @author pcgfd
 */
public class Seleccion {
    // ABRIR EL NIVEL1
    
    private VBox root;
    private HBox selecBox;
    private Button play;
    private ImageView top;
    private ImageView bottom;
    private ImageView arrow1;
    private ImageView arrow2;
    private StackPane center;
    private StackPane bottomStack;
    private ImageView centerI;
    private ImageView carImage; //////
    
    private Auto rojo;
    private Auto amarillo;
    private Auto azul;
    private Auto verde;
    
    
    private Rectangle r_rojo;
    private Rectangle r_amarillo;
    private Rectangle r_azul;
    private Rectangle r_verde;
    public static Player jugador;
    
    public static Rectangle r_auto;
    public static Auto carPlayer;
    public static ArrayList<Rectangle> AUTOS = new ArrayList<>();
    
    private final EventHandler<KeyEvent> handler = (KeyEvent event) -> {
        if(event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT) {
            cambiarAuto(center);
        } 
        event.consume();
    };
    

    public Seleccion(Player jugador) {
        this.jugador = jugador;
        inicializar();
        añadirAutos();
        actions();
    }
    
    private void inicializar(){
        
        play = new Button("Jugar");
        
        play.setOnAction(e-> abrirNivel1());
        arrow1 = new ImageView("/images/left.png");
        arrow2 = new ImageView("/images/right.png");
        top = new ImageView("/images/choose1.jpg");
        bottom = new ImageView("/images/1.jpg");
        centerI = new ImageView("/images/bbb.jpg");
        carImage = new ImageView();
        
        //CARS 
        rojo = new Auto("/images/rojo2.png", null);
        amarillo = new Auto("/images/amarillo.png", null);// Sin coordenada
        azul = new Auto("/images/azul.png", null);
        verde = new Auto("/images/negro.png", null);
        
        //CARS CONTAINERS
        r_rojo = new Rectangle(400,200, new ImagePattern(new Image(rojo.getRutaImagen())));
        r_rojo.setId("red");
        r_amarillo = new Rectangle(400,200, new ImagePattern(new Image(amarillo.getRutaImagen())));
        r_amarillo.setId("yellow");
        r_azul = new Rectangle(400,200, new ImagePattern(new Image(azul.getRutaImagen())));
        r_azul.setId("blue");
        r_verde = new Rectangle(400,200, new ImagePattern(new Image(verde.getRutaImagen())));
        r_verde.setId("green");
        
        
        //SIZES OF IMAGES
        top.setFitHeight(250);
        top.setFitWidth(700);
        
        bottom.setFitHeight(250);
        bottom.setFitWidth(700);
        
        centerI.setFitHeight(200);
        centerI.setFitWidth(400);
        
        carImage.setFitHeight(300);
        carImage.setFitWidth(300);
        
        arrow1.setFitHeight(100);
        arrow1.setFitWidth(100);
        
        arrow2.setFitHeight(100);
        arrow2.setFitWidth(100);
        
        
        bottomStack = new StackPane(bottom, play);
        play.setId("but_new");
        center = new StackPane(centerI, r_amarillo);
        selecBox = new HBox(arrow1, center, arrow2);
        selecBox.setAlignment(Pos.CENTER);
        root = new VBox(top, selecBox, bottomStack);
        
        
    }

    public VBox getRoot() {
        return root;
    }

    
    private void añadirAutos(){
        AUTOS.add(r_rojo);
        AUTOS.add(r_amarillo);
        AUTOS.add(r_azul);
        AUTOS.add(r_verde);
    }
    
    private Rectangle getAuto(){
        Random r = new Random();
        int ra = r.nextInt(AUTOS.size());
        return AUTOS.get(ra);
    }
    
    private void cambiarAuto(StackPane stack){
        stack.getChildren().remove(1);
        stack.getChildren().add(getAuto());
    }
    
    private void actions(){
        arrow1.setOnMouseClicked(e->cambiarAuto(center));
        arrow2.setOnMouseClicked(e->cambiarAuto(center));
    }
    
    

    public EventHandler<KeyEvent> getHandler() {
        return handler;
    }
    
    
    public void abrirNivel1(){
        r_auto = (Rectangle) center.getChildren().get(1);
        if(null != r_auto.getId())switch (r_auto.getId()) {
            case "red":
                carPlayer = rojo;
                break;
            case "blue":
                carPlayer = azul;
                break;
            case "green":
                carPlayer = verde;
                break;
            case "yellow":
                carPlayer = amarillo;
                break;
            default:
                break;
        }else{
            System.out.println("El carro no se seteó");
        }
        
        Nivel nivel = new Nivel();
        Scene s = new Scene(nivel.getRoot(), 900, 900);
        s.getStylesheets().add("/windows/stylesCSS.css");
        s.setOnKeyPressed(nivel.getHandler());
        Main.stage2.setScene(s);
            
    }
    
    
}
