package windows;

import hilos.HiloCarro;
import hilos.HiloPremio;
import hilos.HiloRecompensa;
import hilos.HiloTiempo;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import player.Auto;
import player.Coordenada;
import player.Freezer;
import player.Live;
import player.Obstaculo;
import player.Player;
import player.PremioEspecial;
import player.SuperPower;
import player.ZonaParqueo;
import sound.Sound;

/**
 *
 * @author ff
 */
public class Nivel3 {

    private HiloCarro hc1;
    private HiloCarro hc2;
    private HiloTiempo ht;
    private Thread h_tiempo;
    private Thread h_premio1;
    private Thread h_recom1;
    private Thread h_premio2;
    private Thread h_recom2;
    private Thread h_premio3;
    private Thread h_recom3;
    private Thread tc1;
    private Thread tc2;
    
    private Sound crash;
    private Sound recom;
    private Sound free;
    private Sound lifes;
    private Sound gano;
    private Sound perdio;
    private Sound power;
    private Sound nivel3;

    private Label mensaje;
    private GridPane context;
    private HBox upBox;
    private Label timeLabel;
    private Label lifesLabel;
    private Label PuntajeLabel;
    private Label avisosLabel;
    private Label timeLabel2;
    private Label lifesLabel2;
    private Label PuntajeLabel2;
    private Label title;
    private HBox timeBox;
    private HBox lifeBox;
    private HBox PuntajeBox;
    private VBox playerInfo;
    private VBox timeInfo;
    private VBox layoutRoot;
    private ZonaParqueo zona;
    private Rectangle r_zona;
    private Rectangle r_auto1;
    private Rectangle r_auto2;
    
    

    private int puntaje;
    private int vidas;
    private int tiempo;

    private ArrayList<Coordenada> posiciones;
    private ArrayList<Rectangle> fig_ob;
    private ArrayList<Obstaculo> obstaculos;
    private ArrayList<PremioEspecial> premiosEspeciales;

    //ELEMENTOS DEL JUGADOR. PUNTO DE PARTIDA: 0 x 50
    private Player jugador;
    private Auto autoJugador;
    private Rectangle r_jugador;
    private Coordenada posJugador;
    private Coordenada posA1;
    private Coordenada posA2;

    private final EventHandler<KeyEvent> handler = (KeyEvent event) -> {
        if (null != event.getCode()) {
            switch (event.getCode()) {
                case RIGHT:
                    r_jugador.setTranslateX(r_jugador.getTranslateX() + 10);
                    choqueObs();
                    break;
                case LEFT:
                    r_jugador.setTranslateX(r_jugador.getTranslateX() - 10);
                    choqueObs();
                    break;
                case UP:
                    r_jugador.setTranslateY(r_jugador.getTranslateY() - 10);
                    choqueObs();
                    break;
                case DOWN:
                    r_jugador.setTranslateY(r_jugador.getTranslateY() + 10);
                    choqueObs();
                    break;
                default:
                    break;
            }
        }
        event.consume();
    };

    public Nivel3() {
        this.jugador = Seleccion.jugador;
        getSounds();
        encerar();
        crearZona();
        setCoorJugador();
        inicializar();
        asignarPosOb(10);
        crearHiloT();
        generarHilos();
    }
    
    private void getSounds(){
        crash = new Sound("crash.mp3");
        recom = new Sound("recom.mp3");
        free = new Sound("free.mp3");
        lifes = new Sound("heart.mp3");
        gano = new Sound("win.mp3");
        perdio = new Sound("lose.mp3");
        power = new Sound("power.mp3");
        nivel3 = new Sound("nivel3.mp3");
    }

    protected final void inicializar() {
        nivel3.play();
        r_auto1 = new Rectangle(100, 70, new ImagePattern(new Image("/images/amarillo.png")));
        r_auto2 = new Rectangle(100, 70, new ImagePattern(new Image("/images/amarillo.png")));
        r_auto1.setId("obstaculo");
        r_auto1.setArcWidth(30);
        r_auto1.setArcHeight(30);
        r_auto1.setStroke(Color.RED);
        fig_ob.add(r_auto1);
        
        r_auto2.setId("obstaculo");
        r_auto2.setArcWidth(30);
        r_auto2.setArcHeight(30);
        r_auto2.setStroke(Color.RED);
        fig_ob.add(r_auto1);
        
        mensaje = new Label();
        mensaje.setId("title_main");
        context = new GridPane();
        title = new Label("Nivel 2");
        title.setId("title_main");
        timeLabel = new Label("Tiempo: ");
        timeLabel.setId("title_main");
        PuntajeLabel = new Label("Puntaje: ");
        PuntajeLabel.setId("title_main");
        lifesLabel = new Label("Vidas: ");
        lifesLabel.setId("title_main");

        timeLabel2 = new Label();
        timeLabel2.setId("title_main");
        PuntajeLabel2 = new Label();
        PuntajeLabel2.setId("title_main");
        PuntajeLabel2.setText(String.valueOf(puntaje));
        lifesLabel2 = new Label();
        lifesLabel2.setId("title_main");
        lifesLabel2.setText(String.valueOf(vidas));

        

        lifeBox = new HBox(lifesLabel, lifesLabel2);
        PuntajeBox = new HBox(PuntajeLabel, PuntajeLabel2);
        timeBox = new HBox(timeLabel, timeLabel2);
        playerInfo = new VBox(lifeBox, PuntajeBox, mensaje);
        timeInfo = new VBox(title, timeBox);

        upBox = new HBox(playerInfo, timeInfo);
        upBox.setId("upper_lay");

        context.setId("pane");
        context.add(r_jugador,0,0);
        context.add(r_zona, (int) zona.getPosicion().getX(), (int) zona.getPosicion().getY());
        context.add(r_auto1, (int)posA1.getX(), (int)posA1.getY());
        context.add(r_auto2, (int)posA2.getX(), (int)posA1.getY());

        r_jugador.setWidth(80);
        r_jugador.setHeight(70);
        r_jugador.setArcWidth(30);
        r_jugador.setArcHeight(30);
        r_jugador.setStroke(Color.BLACK);
        

        layoutRoot = new VBox(upBox, context);
        layoutRoot.setStyle("-fx-background-image: url(/images/paneb.jpg)");

    }

    private Coordenada getRandomPos() {

        int x = randomNumberInRange(1, 8);
        int y = randomNumberInRange(0, 8);
        Coordenada c = new Coordenada(x, y);
        System.out.println(c);
        while (posiciones.contains(c)) {
            System.out.println("Dentro");
            System.out.println("Coord actual: " + c);
            x = randomNumberInRange(1, 8);
            y = randomNumberInRange(0, 8);
            c.setX(x);
            c.setY(y);
        }
        posiciones.add(c);

        return c;

    }

    protected VBox getRoot() {
        return layoutRoot;
    }

    private void asignarPosOb(int n) {
        System.out.println("Asignando posiciones .....");
        for (int i = 0; i < n; i++) {
            String ruta = randomRuteObs();
            Coordenada coor = getRandomPos();
            Rectangle r = new Rectangle(100, 100, new ImagePattern(new Image(ruta)));
            r.setArcWidth(20);
            r.setArcHeight(20);
            r.setStroke(Color.RED);
            r.setId("obstaculo");
            Obstaculo ob = new Obstaculo(ruta, coor);
            obstaculos.add(ob);

            fig_ob.add(r);

            context.add(r, (int) coor.getX(), (int) coor.getY());
        }

        Freezer f = new Freezer("/images/freezer.png", getRandomPos());
        Live l = new Live("/images/corazon.png", getRandomPos());
        SuperPower sp = new SuperPower("/images/superpower.png", getRandomPos());
        premiosEspeciales.add(f);
        premiosEspeciales.add(l);
        premiosEspeciales.add(sp);
    }

    private String randomRuteObs() {
        System.out.println("randomRoute......");
        String[] rutasO = {"/images/gato.png", "/images/cubo2.png", "/images/pelota.png", "/images/sem.png"};
        Random r = new Random();
        int al = r.nextInt(4);
        return rutasO[al];
    }

    public EventHandler<KeyEvent> getHandler() {
        return handler;
    }

    private void choqueObs() {
        System.out.println("choqueObs......");
        for (Rectangle r : fig_ob) {
            if (r_jugador.getBoundsInParent().intersects(r.getBoundsInParent())) {

                switch (r.getId()) {
                    case "obstaculo":

                        if (!Nivel.superp) {
                            lifesLabel2.setText(String.valueOf(--vidas));
                            System.out.println("Choque");
                            crash.play();
                            r_jugador.setTranslateX(0);
                            r_jugador.setTranslateY(50);
                        }

                        if (vidas == 0) {
                            perdio.play();
                            guardarDatosJugador();
                            guardarArchivo();
                            lose();
                        }
                        break;
                    case "puntos":
                        recom.play();
                        puntaje += 100;
                        PuntajeLabel2.setText(String.valueOf(puntaje));
                        context.getChildren().remove(r);
                        Nivel.quitar2 = true;
                        break;
                    case "vida":
                        lifes.play();
                        System.out.println("Vida");
                        lifesLabel2.setText(String.valueOf(++vidas));
                        
                        mensaje.setText("¡Aumentan tus vidas!");
                        context.getChildren().remove(r);
                        Nivel.quitar = true;
                        break;
                    case "freezer":
                        free.play();
                        Nivel.interrupt = true;
                        mensaje.setText("¡Freezer activado!");
                        context.getChildren().remove(r);
                        Nivel.quitar = true;
                        break;
                    case "sp":
                        power.play();
                        Nivel.superp = true;
                        mensaje.setText("¡Super poder activado!");
                        context.getChildren().remove(r);
                        Nivel.quitar = true;
                        break;
                    case "zona":
                        gano.play();
                        guardarDatosJugador();
                        guardarArchivo();
                        win(jugador);
                        break;
                    default:
                        break;
                }

            }

        }
    }

    private PremioEspecial getRandomPre() {
        System.out.println("GetRandomPre.....");
        Random r = new Random();
        int x = r.nextInt(3);
        return premiosEspeciales.get(x);

    }

    private void generarHilos() {
        System.out.println("Generando hilos.... ");
        h_premio1 = new Thread(new HiloPremio(context, getRandomPre(), fig_ob));
        h_recom1 = new Thread(new HiloRecompensa(getRandomPos(), context, fig_ob));
        h_premio2 = new Thread(new HiloPremio(context, getRandomPre(), fig_ob));
        h_premio3 = new Thread(new HiloPremio(context, getRandomPre(), fig_ob));
        h_recom2 = new Thread(new HiloRecompensa(getRandomPos(), context, fig_ob));
        h_recom3 = new Thread(new HiloRecompensa(getRandomPos(), context, fig_ob));

        h_premio1.start();
        h_recom1.start();
        h_premio2.start();
        h_premio3.start();
        h_recom2.start();
        h_recom3.start();

    }

    public static int randomNumberInRange(int min, int max) {
        System.out.println("Randon range....");
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }

    private void win(Player jugador) {
        nivel3.stop();
        System.out.println(jugador);
        h_tiempo.stop();
        tc1.stop();
        tc2.stop();
        Alert a = new Alert(Alert.AlertType.INFORMATION);
        a.setTitle("Ganador");
        a.setHeaderText("Has completado el juego. ¡FELICIDADES!");
        a.setContentText("Jugador: " + jugador.getNombre() + "\nPuntaje: " + jugador.getPuntaje() + "\nTiempo: " + jugador.getTiempo());

        // option != null.
        Optional<ButtonType> option = a.showAndWait();

        if (option.get() == ButtonType.OK) {
           Main.stage2.close();
        }
    }

    private void lose() {
        System.out.println(jugador);
        nivel3.stop();
        h_tiempo.stop();
        tc1.stop();
        tc2.stop();
        System.out.println("perdio....");
        Stage st = new Stage();
        GameOver go = new GameOver(st, Main.stage2);

        Scene s = new Scene(go.getRoot(), 500, 500);
        st.setScene(s);
        st.show();
    }

    private final void crearZona() {
        System.out.println("Creando zona....");
        zona = new ZonaParqueo("/images/zona.png", new Coordenada(6, 6));
        r_zona = new Rectangle(70, 70, new ImagePattern(new Image(zona.getRutaImagen())));
        r_zona.setId("zona");
        fig_ob.add(r_zona);
        posiciones.add(zona.getPosicion());
    }

    private void setCoorJugador() {
        System.out.println("Seteando jugador....");
        autoJugador.setPosicion(posJugador);
    }

    private void guardarDatosJugador() {
        jugador.setPuntaje(jugador.getPuntaje() +puntaje);
        jugador.setTiempo(jugador.getTiempo() +tiempo);
        jugador.setAuto(autoJugador);
    }

    private void guardarArchivo() {
        try (PrintWriter p = new PrintWriter(new FileWriter("jugadores.txt", true), false)) {
            System.out.println("Guardando jugador " + jugador.getNombre());
            p.println(jugador.toString());

        } catch (IOException ex) {
            System.err.println("No se guardó el jugador");
        }
    }

    private void crearHiloT(){
        ht =new HiloTiempo(timeLabel2, puntaje);
        h_tiempo = new Thread(ht);
        h_tiempo.start();
        hc1 = new HiloCarro(context,r_auto1, true, posA1);
        hc2 = new HiloCarro(context, r_auto2, false, posA2);
        tc1 = new Thread(hc1);
        tc2 = new Thread(hc2);
        tc1.start();
        tc2.start();
    }
    
    public final void encerar(){
        vidas = 3;
        puntaje = 0;
        autoJugador = Seleccion.carPlayer;
        r_jugador = Seleccion.r_auto;
        posJugador = new Coordenada(0, 0);
        posiciones = new ArrayList<>();
        fig_ob = new ArrayList<>();
        obstaculos = new ArrayList<>();
        premiosEspeciales = new ArrayList<>();
        posA1 = new Coordenada(8, 0);
        posA2 = new Coordenada(6, 4);
        
        posiciones.add(posJugador);
        posiciones.add(posA1);
        posiciones.add(posA2);
        
        
    }
    
   
}