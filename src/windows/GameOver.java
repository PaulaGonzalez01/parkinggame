/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author pcgfd
 */
public class GameOver {
    private VBox root;
    private ImageView image;
    private Button salir;

    public GameOver(Stage stage, Stage s2) {
        image = new ImageView("/images/over.jpg");
        salir = new Button("SALIR");
        salir.setId("but_new");
        salir.setOnAction(e-> closeWindows(stage, s2));
        root = new VBox(image, salir);
        root.setId("root_new");
        root.setAlignment(Pos.CENTER);
    }

    public VBox getRoot() {
        return root;
    }
    
    public void closeWindows(Stage stage, Stage s2){
        stage.close();
        s2.close();
    }
    
}
