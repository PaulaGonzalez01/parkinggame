/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sound.Sound;

/**
 *
 * @author ff
 */
public class Main {
    private VBox root;
    private Button user;
    private Button historial;
    private Sound intro;
    private Button exit;
    private ImageView title;
    public static Stage stage2;
    
    

    public Main() {
        inicializar();
        giveActions();
        stage2.setResizable(true);
    }
    
    private void inicializar(){
        intro = new Sound("intro.mp3");
        intro.play();
        exit = new Button("Salir");
        exit.setId("but_new");
        historial = new Button("Historial");
        historial.setId("but_new");
        user = new Button("Nuevo Juego");
        user.setId("but_new");
        title = new ImageView("/images/title.jpg");
        title.setFitHeight(60);
        title.setFitWidth(400);
        
        root = new VBox(title, user, historial, exit);
        root.setAlignment(Pos.CENTER);
        root.setSpacing(50);
        root.setId("root_main");
        stage2 = new Stage();
        
    }
    

    public VBox getRoot() {
        return root;
    }
    
    public void nuevoUsuario(){
        intro.stop();
        NewUser venUsu = new NewUser();
        Scene es = new Scene(venUsu.getRoot(), 700, 700);
        es.getStylesheets().add("/windows/stylesCSS.css");
        
        stage2.setScene(es);
        stage2.setTitle("NUEVO JUEGO");
        //stage2.setResizable(true);
        stage2.show();
    }
    public void mostrarHistorial(){
        intro.stop();
        Historial his = new Historial();
        Scene es = new Scene(his.getRoot(), 800, 500);
        es.getStylesheets().add("/windows/stylesCSS.css");
        stage2.setScene(es);
        stage2.setTitle("HISTORIAL");
        //stage2.setResizable(true);
        stage2.show();
    }
    public void salir(){
        intro.stop();
        Platform.exit();
    }
    
    private void giveActions(){
        user.setOnAction(e-> nuevoUsuario());
        historial.setOnAction(e -> mostrarHistorial());
        exit.setOnAction(e-> salir());
    }
    
}
