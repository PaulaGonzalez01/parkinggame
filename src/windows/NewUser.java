/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package windows;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import player.Player;

/**
 *
 * @author ff
 */
public class NewUser {

    private Date fecha;
    private VBox root;
    private HBox container;
    private Label nameLabel;
    public static TextField nameField;
    private Button play;
    private Player jugador;

    public NewUser() {
        inicialize();
    }

    private void inicialize() {

        //ORGANIZE NODES
        nameLabel = new Label("Nombre: ");
        nameLabel.setId("name_new");
        nameField = new TextField();
        nameField.setId("txt_field");
        play = new Button("Jugar");
        play.setId("but_new");
        play.setOnAction(e -> abrirVentana());
        container = new HBox(50, nameLabel, nameField);
        container.setAlignment(Pos.CENTER);
        root = new VBox(50, container, play);
        root.setAlignment(Pos.CENTER);
        root.setId("root_new");

    }

    public VBox getRoot() {
        return root;
    }

    public void guardarJuagdor() {
        jugador = new Player(nameField.getText());
        try (PrintWriter p = new PrintWriter(new FileWriter("jugadores.txt", true), false)) {
            System.out.println("Guardando jugador " + jugador.getNombre());
            p.println(jugador.toString());

            abrirVentana();

        } catch (IOException ex) {
            System.err.println("No se guardó el jugador");
        }
    }

    public void abrirVentana() {
        jugador = new Player(nameField.getText());
        Seleccion sel = new Seleccion(jugador);
        Scene es = new Scene(sel.getRoot(), 700, 700);
        es.getStylesheets().add("/windows/stylesCSS.css");
        es.setOnKeyPressed(sel.getHandler());
        Main.stage2.setScene(es);
    }

}
