/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parkinggame;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;
import windows.Main;

/**
 *
 * @author ff
 */
public class parkingGUI  extends Application{
    @Override
    public void start(Stage primaryStage) {
        
        Main mainW = new Main();
        Scene scene = new Scene(mainW.getRoot(), 500, 500);
        scene.getStylesheets().add("/windows/stylesCSS.css");
        primaryStage.setTitle("PARKING GAME");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
