/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.ArrayList;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import player.Coordenada;
import player.Recompensa;
import windows.Nivel;

/**
 *
 * @author pcgfd
 */
public class HiloRecompensa implements Runnable {

    private Recompensa reward;
    private GridPane pane;
    private Rectangle rect;
    private ArrayList<Rectangle> fig_ob;

    public HiloRecompensa(Coordenada pos, GridPane pane, ArrayList<Rectangle> fig_ob) {
        this.reward = new Recompensa("/images/recompensa.png", pos);
        this.pane = pane;
        this.fig_ob = fig_ob;
        rect = new Rectangle(70, 70, new ImagePattern(new Image(reward.getRutaImagen())));
//        rect.setX(reward.getPosicion().getX());
//        rect.setY(reward.getPosicion().getY());
        rect.setArcWidth(20);
        rect.setArcHeight(20);
        rect.setStroke(Color.RED);
        rect.setId("puntos");

    }

    @Override
    public void run() {
        int i = reward.getTiempo();
        while (i > 0) {
            try {
                if (i == reward.getTiempo()) {
                    Platform.runLater(() -> añadirRec());
                } else if (i == 1 || Nivel.quitar2) {
                    Platform.runLater(() -> quitar());
                    Nivel.quitar2 = false;
                }

                Thread.sleep(1000);
                i--;

            } catch (Exception ex) {
                System.err.println("Interrupción en los segundos");
            }
        }
    }

    private void añadirRec() {
        pane.add(rect, (int) reward.getPosicion().getX(),(int) reward.getPosicion().getY());
        fig_ob.add(rect);
    }

    private void quitar() {
        pane.getChildren().remove(rect);
        fig_ob.remove(rect);
    }

}
