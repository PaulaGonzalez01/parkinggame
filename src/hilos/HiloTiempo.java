/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import player.Player;
import sound.Sound;
import windows.GameOver;
import windows.Main;
import windows.Nivel;
import windows.Seleccion;

/**
 *
 * @author pcgfd
 */
public class HiloTiempo implements Runnable {

    public int interval = 120;
    private int tiempo;
    Label timeLabel;
    Player jugador;
    private Sound perdio;

    public HiloTiempo(Label timeLabel, int puntaje) {
        this.timeLabel = timeLabel;
        jugador = Seleccion.jugador;
        perdio = new Sound("lose.mp3");
    }

    @Override
    public void run() {
        int i = 120;
        while (i > 0) {
            try {
                if (i == 1) {
                    perdio.play();
                    guardarDatosJugador(i, tiempo);
                    guardarArchivo();
                    Platform.runLater(() -> lose());
                    break;
                }else if(Nivel.interrupt){
                    Nivel.interrupt = false;
                    Thread.sleep(9000);
                }
                Thread.sleep(1000);
                interval--;
                tiempo = interval;
                i--;
                System.out.println(i);
                Platform.runLater(() -> setTime(interval));

            } catch (Exception ex) {
                System.err.println("Interrupción en los segundos");
                return;
            }
        }
    }

    public void setTime(int i) {
        timeLabel.setText(String.valueOf(i));
    }

    public void lose() {
        System.out.println("perdio....");
        Stage st = new Stage();
        GameOver go = new GameOver(st, Main.stage2);

        Scene s = new Scene(go.getRoot(), 500, 500);
        st.setScene(s);
        st.show();
    }
    public int getTiempo(){
        return tiempo;
    }
    
    private void guardarArchivo() {
        try (PrintWriter p = new PrintWriter(new FileWriter("jugadores.txt", true), false)) {
            System.out.println("Guardando jugador " + jugador.getNombre());
            p.println(jugador.toString());

        } catch (IOException ex) {
            System.err.println("No se guardó el jugador");
        }
    }
    
    private void guardarDatosJugador(int puntaje, int tiempo) {
        jugador.setPuntaje(jugador.getPuntaje() + puntaje);
        jugador.setTiempo(jugador.getTiempo() + tiempo);
    }
}
