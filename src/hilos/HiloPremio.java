/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.ArrayList;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import player.Freezer;
import player.Live;
import player.PremioEspecial;
import player.SuperPower;
import windows.Nivel;

/**
 *
 * @author pcgfd
 */
public class HiloPremio implements Runnable {

    private PremioEspecial reward;
    private GridPane pane;
    private Rectangle rect;
    private  ArrayList<Rectangle> fig_ob;

    public HiloPremio(GridPane pane, PremioEspecial p, ArrayList<Rectangle> fig_ob) {
        this.fig_ob = fig_ob;
        this.reward = p;
        this.pane = pane;
        rect = new Rectangle(70, 70, new ImagePattern(new Image(reward.getRutaImagen())));
//        rect.setX(reward.getPosicion().getX());
//        rect.setY(reward.getPosicion().getY());
        if (p instanceof Live) {
            System.out.println("vidaaaaa hilo");
            rect.setId("vida");
        } else if (p instanceof Freezer) {
            rect.setId("freezer");
        } else if (p instanceof SuperPower) {
            rect.setId("sp");
        }

    }
    
    @Override
    public void run() {
        int i =reward.getTiempo();
       
        while(i>0){
           try {
               if(i == reward.getTiempo()){
                   Platform.runLater(()->añadirRec());
               }else if(i == 1 || Nivel.quitar){
                   Platform.runLater(()-> quitar());
                   Nivel.quitar = false;
                   Nivel.superp =false;
               }
               
               Thread.sleep(1000);
               i--;
                          
               
           } catch (Exception ex) {
               System.err.println("Interrupción en los segundos");
           }
       }
    }
    
    public void añadirRec(){
        pane.add(rect, (int) reward.getPosicion().getX(),(int) reward.getPosicion().getY());
        fig_ob.add(rect);
    }
    
    public void quitar(){
        pane.getChildren().remove(rect);
        fig_ob.remove(rect);
    }
    
   
    
}
