/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import javafx.application.Platform;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;
import player.Coordenada;

/**
 *
 * @author pcgfd
 */
public class HiloCarro implements Runnable {

    private int tiempo;
    private Rectangle auto;
    private int limit;
    private boolean vertical;
    private GridPane grid;
    private Coordenada pos;

    public HiloCarro(GridPane grid, Rectangle auto, boolean vertical, Coordenada pos) {
        this.auto = auto;
        tiempo = 0;
        limit = 100;
        this.vertical = vertical;
        this.grid = grid;
        this.pos = pos;
    }
    

    @Override
    public void run() {
        int cont = 0;
        int c2 = 0;
        int i = 120;
        while (i > 0) {
            try {
//                Platform.runLater(() -> ubicar());
                Thread.sleep(1000);
                i--;
                if(cont == 10 || c2<10){
                    Platform.runLater(() -> moverCarroIA());
                    cont = 0;
                    c2++;
                }else if(cont<10 || c2 == 10){
                    Platform.runLater(() -> moverCarroDAB());
                    c2 = 0;
                    cont++;
                }
                

            } catch (Exception ex) {
                System.err.println("Interrupción en los segundos");
                return;
            }
        }
    }
    
    private void moverCarroIA(){
        
        if(vertical){
            auto.setTranslateY(auto.getTranslateY() + 10);
        }else{
           auto.setTranslateX(auto.getTranslateX() - 10); 
        }
        
        
    }
    private void moverCarroDAB(){
        
        if(vertical){
            auto.setTranslateY(auto.getTranslateY() - 10);
        }else{
           auto.setTranslateX(auto.getTranslateX() + 10); 
        }
        
    }
    
    private void ubicar(){
        grid.add(auto, (int)pos.getX(), (int)pos.getY());
    }
    

}
